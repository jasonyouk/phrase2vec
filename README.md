##############################
### Phrase2Vec in Practice : How to calculate the semantic similarity between two phrases
##############################

This is the code for Aerin Kim's talk at AI With The Best (http://ai.withthebest.com) conference.

Pretrained Word Vector to download: https://drive.google.com/file/d/0B7XkCwpI5KDYNlNUTTlSS21pQmM/edit

Talk Video (2016 Fall): https://youtu.be/kGGA1Wm8_x0